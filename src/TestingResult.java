
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author kng
 */
public class TestingResult {

    private final boolean successful;

    private final String errorMessage;

    private final List<QueryResult> queryResults;

    public TestingResult(boolean successful, String errorMessage, List<QueryResult> queryResults) {
        this.successful = successful;
        this.errorMessage = errorMessage;
        this.queryResults = queryResults;
    }

    /**
     * @return the successful
     */
    public boolean isSuccessful() {
        return successful;
    }

    /**
     * @return the errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * @return the queryResults
     */
    public List<QueryResult> getQueryResults() {
        return queryResults;
    }
}
