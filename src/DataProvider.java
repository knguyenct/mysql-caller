
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kng
 */
public class DataProvider {

    private final Connection connection;

    public DataProvider(Connection connection) {
        this.connection = connection;
    }

    /**
     *
     * @param query
     * @return
     */
    public QueryResult executeNonQuery(String query) {
        long time;
        try {
            try (Statement stmt = (Statement) this.connection.createStatement()) {
                time = System.currentTimeMillis();
                stmt.execute(query);
                time = System.currentTimeMillis() - time;
            }
        } catch (SQLException sqle) {
            Logger.getLogger(Testing.class.getName()).log(Level.SEVERE, null,
                    sqle);
            return new QueryResult(query, 0, null, false, sqle.getErrorCode(),
                    sqle.getMessage());
        }
        return new QueryResult(query, time, null, true, 0, "");
    }

    /**
     *
     * @param query
     * @return
     */
    public QueryResult executeQuery(String query) {
        long time;
        ResultSet resultSet;
        try {
            try (Statement stmt = (Statement) this.connection.createStatement()) {
                time = System.currentTimeMillis();
                resultSet = stmt.executeQuery(query);
                time = System.currentTimeMillis() - time;
            }
        } catch (SQLException sqle) {
            Logger.getLogger(Testing.class.getName()).log(Level.SEVERE, null,
                    sqle);
            return new QueryResult(query, 0, null, false, sqle.getErrorCode(),
                    sqle.getMessage());
        }
        return new QueryResult(query, time, resultSet, true, 0, "");
    }
}
