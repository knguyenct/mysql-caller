
import java.sql.ResultSet;

/**
 *
 * @author kng
 */
public class QueryResult {

    private final String queryString;

    private final long time;

    private final ResultSet resultSet;

    private final boolean successful;

    private final String message;

    private final int errorCode;

    public QueryResult(String queryString, long time, ResultSet resultSet,
            boolean successful, int errorCode, String message) {
        this.queryString = queryString;
        this.time = time;
        this.resultSet = resultSet;
        this.successful = successful;
        this.message = message;
        this.errorCode = errorCode;
    }

    /**
     * @return the time
     */
    public long getTime() {
        return time;
    }

    /**
     * @return the resultSet
     */
    public ResultSet getResultSet() {
        return resultSet;
    }

    /**
     * @return the successful
     */
    public boolean isSuccessful() {
        return successful;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @return the errorCode
     */
    public int getErrorCode() {
        return errorCode;
    }

    /**
     * @return the queryString
     */
    public String getQueryString() {
        return queryString;
    }
}
