
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author kng
 */
public class Testing {

    private final MysqlDataSource dataSource;

    private final String[] keywords = new String[]{
        "DROP",
        "CREATE",
        "USE",
        "INSERT",
        "UPDATE",
        "DELETE",
        "SELECT",
        "SET",
        "ROLLBACK",
        "START"
    };

    public Testing(String serverName, int port, String username, String password) {
        this.dataSource = new MysqlDataSource();
        this.dataSource.setServerName(serverName);
        this.dataSource.setPort(port);
        this.dataSource.setUser(username);
        this.dataSource.setPassword(password);
    }

    private boolean containKeyword(String text) {
        for (String s : this.keywords) {
            if (text.indexOf(s) == 0) {
                return true;
            }
        }
        return false;
    }

    public TestingResult runTest(String testFile) {
        List<QueryResult> queryResults = null;
        try {
            queryResults = new LinkedList<>();
            try (Connection conn = (Connection) this.dataSource.getConnection()) {
                DataProvider dp = new DataProvider(conn);
                try (BufferedReader br = new BufferedReader(new FileReader(testFile))) {
                    String line;
                    while ((line = br.readLine()) != null) {
                        // process the line.
                        if (this.containKeyword(line)) {
                            QueryResult qr = dp.executeNonQuery(line);
                            if (qr != null) {
                                queryResults.add(qr);
                            }
                        }
                    }
                }
            }
            return new TestingResult(true, "", queryResults);
        } catch (IOException ioe) {
            Logger.getLogger(Testing.class.getName()).log(Level.SEVERE, null, ioe);
            return new TestingResult(false, "Error when openning file.", queryResults);
        } catch (SQLException sqle) {
            Logger.getLogger(Testing.class.getName()).log(Level.SEVERE, null, sqle);
            return new TestingResult(false, "Error when connecting to MySQL.", queryResults);
        }
    }

    public boolean testConnection() {
        try {
            Connection conn = (Connection) this.dataSource.getConnection();
            conn.close();
            return true;

        } catch (SQLException ex) {
            Logger.getLogger(DataProvider.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
